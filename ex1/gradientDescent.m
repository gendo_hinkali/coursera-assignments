function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESCENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCost) and gradient here.
    tmptheta = [theta(1),theta(2)];
	for j=1:2
       tmp = 0;
       for i=1:m
            tmp = tmp + (hyp(tmptheta,X(i,2)) - y(i)) * X(i,j);
       endfor
	   theta(j) = theta(j) - alpha/m * tmp;
	endfor
	   
    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCost(X, y, theta);
endfor

endfunction

function h = hyp(theta,X)
	h = theta(1) + theta(2)*X;
endfunction
